package com.example.CrudProject.Service.Impl;

import com.example.CrudProject.Model.Car;
import com.example.CrudProject.Repositor.CarRepository;
import com.example.CrudProject.Service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;


    @Override
    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    @Override
    public Car getCarById(int id) {
        return carRepository.findById(id).orElse(null);
    }

    @Override
    public Car save(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Car update(Car car , int id) {
        carRepository.findById(id)
                .ifPresent(car1 -> {
                    car1.setId(car.getId());
                    car1.setName(car.getName());
                    car1.setModel(car.getModel());
                    car1.setColor(car.getColor());
                    car1.setEngineType(car.getEngineType());
                });
        return car;
    }
    @Override
    public void deleteById(int id) {
        carRepository.deleteById(id);

    }
}
