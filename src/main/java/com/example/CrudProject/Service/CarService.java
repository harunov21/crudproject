package com.example.CrudProject.Service;

import com.example.CrudProject.Model.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {

    Car save(Car car);

    Car update(Car car , int id);

    void deleteById(int id);


    List<Car> getAllCars();

    Car getCarById(int id);
}
