package com.example.CrudProject;

import com.example.CrudProject.Configuration.Properties;
import com.example.CrudProject.Controller.CarController;
import com.example.CrudProject.Model.Car;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class CrudProjectApplication implements CommandLineRunner {


	private final Properties properties;
	public static void main(String[] args) {
		SpringApplication.run(CrudProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

			List<Car> cars = properties.getCars();

			for (Car car : cars) {
				System.out.println("Car Name: " + car.getName());
				System.out.println("Car Model: " + car.getModel());
				System.out.println("Car Color: " + car.getColor());
				System.out.println("Car Engine Type: " + car.getEngineType());
		}
	}
}







